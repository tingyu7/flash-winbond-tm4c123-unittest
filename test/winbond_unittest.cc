#include <iostream>
#include <algorithm>
#include "../samples/winbond.h"
#include "gtest/gtest.h"

using ::testing::TestWithParam;
using ::testing::Range;

// ****************************************************************************
//
// Winbond Write Unit Test
//
// ****************************************************************************
class WinbondWriteTest : public ::testing::TestWithParam<int> {
	protected:
		// You can implement all the usual fixture class members here.
		// To access the test parameter, call GetParam() from class
		// TestWithParam<T>.
		virtual void SetUp() {
			std::fill(mem, mem + 1024, 0xFF);
			std::fill(ref_mem, ref_mem + 1024, 0xFF);
		}

		virtual void TearDown() {}

		void construct_ref_mem(uint32_t addr, 
						       uint32_t num_byte, 
						       uint8_t *data_to_write, 
						       uint8_t *mem_to_write) {
			int final_addr = addr + num_byte;

			int i;
		    for (i = addr; i < final_addr; i++) {
		        mem_to_write[i] = data_to_write[i - addr];
		    }  
		}

		// Visualize virtual flash memory
		void print_mem(uint8_t *mem_buf) {
			uint8_t i, j, k;
			uint32_t addr = 0;
			for (k = 0; k < 4; k++) {
				for (i = 0; i < 16; i++) {
					for (j = 0; j < 16; j++) {
						printf("%02x ", mem_buf[i * 16 + j + addr]);
					}
						printf("\n");
				}
				addr += 256;
				// put a space between pages
				printf("\n");
			}	
		}
};

uint8_t mem[1024]; // extern mem, initialized content in constructor
uint8_t ref_mem[1024];

// TEST_P(WinbondWriteTest, AddressWrapping) {
// 	// Testing the address wrapping feature
// 	// Prove that address wrapping can occur if trying to write more
// 	// than 256 bytes of data to a page
// 	// Prove that virtual flash has simulated real flash well

// 	// Write 200 bytes of data starting at address 128
// 	// The bottom half and the first 72 bytes of page 0 should
// 	// be 0x00 (data written is warpped around)
// 	uint8_t src[200] = {0x00};
// 	write_page(128, 200, src);

// 	// Construct reference mem to compare to
// 	// construct_ref_mem(128, 200, src, ref_mem);

// 	int i;
//     for (i = 0; i < 72; i++) {
//         ref_mem[i] = 0x00;
//     }

//     for (i = 128; i < 256; i++) {
//         ref_mem[i] = 0x00;
//     }    

// 	// print_mem(mem);
// 	// print_mem(ref_mem);

// 	EXPECT_EQ(mem[GetParam()], ref_mem[GetParam()]);
// }

// TEST_P(WinbondWriteTest, AllignedUnderOnePage) {
// 	// Write 128 bytes of data starting at address 256 (Start of Block 1)
// 	uint8_t src[128] = {0x00};
// 	my_spi_write(256, 128, src);

// 	// Construct reference mem to compare to
// 	construct_ref_mem(256, 128, src, ref_mem);

// 	// print_mem(mem);
// 	// print_mem(ref_mem);

// 	EXPECT_EQ(mem[GetParam()], ref_mem[GetParam()]);
// }

// TEST_P(WinbondWriteTest, AllignedMoreThanOnePage) {
// 	// Write 384 bytes of data starting at address 512 (Start of Block 2)
// 	uint8_t src[384] = {0x00};
// 	my_spi_write(512, 384, src);

// 	// Construct reference mem to compare to
// 	construct_ref_mem(512, 384, src, ref_mem);

// 	// print_mem(mem);
// 	// print_mem(ref_mem);

// 	EXPECT_EQ(mem[GetParam()], ref_mem[GetParam()]);
// }

// TEST_P(WinbondWriteTest, NotAllignedSpaceBetweenStartAndUpperPageBound) {
// 	// Write 120 bytes of data starting at address 640 (Mid of Block 2)
// 	uint8_t src[120] = {0x00};
// 	my_spi_write(640, 120, src);

// 	// Construct reference mem to compare to
// 	construct_ref_mem(640, 120, src, ref_mem);

// 	// print_mem(mem);
// 	// print_mem(ref_mem);

// 	EXPECT_EQ(mem[GetParam()], ref_mem[GetParam()]);
// }

// TEST_P(WinbondWriteTest, NotAllignedNoSpaceBetweenStartAndUpperPageBound) {
// 	// Write 512 bytes of data starting at address 128 (Mid of Block 0)
// 	uint8_t src[512] = {0x00};
// 	my_spi_write(128, 512, src);

// 	// Construct reference mem to compare to
// 	construct_ref_mem(128, 512, src, ref_mem);

// 	// print_mem(mem);
// 	// print_mem(ref_mem);

// 	EXPECT_EQ(mem[GetParam()], ref_mem[GetParam()]);
// }

// INSTANTIATE_TEST_CASE_P(SetupFlashAddressArray,
//                         WinbondWriteTest,
//                         Range(0, 1024));

// // ****************************************************************************
// //
// // Winbond Read Unit Test
// //
// // ****************************************************************************

// // Created to allow different set of Value Parameters
// class WinbondReadTest : public WinbondWriteTest {
// 	virtual void SetUp() {
// 		std::fill(read_buf, read_buf + 1024, 0xFF); // fill with all 0xFF
// 	}

// 	virtual void TearDown() {}
// };

// uint8_t read_buf[1024];

// // Test to show that virtual flash read has simulated real flash read well
// TEST_P(WinbondReadTest, ReadPage) {
// 	uint8_t src[512] = {0x00};
// 	uint8_t read_buf[1024]; // my_spi_read reads to here

// 	my_spi_write(128, 512, src);
// 	my_spi_read(128, 512, read_buf);

// 	// print_mem(mem);
// 	// print_mem(read_buf);

// 	EXPECT_EQ(mem[GetParam() + 128], read_buf[GetParam()]);
// }

// INSTANTIATE_TEST_CASE_P(SetupFlashAddressArray,
//                         WinbondReadTest,
//                         Range(0, 512));

// // ****************************************************************************
// //
// // Winbond Block Erase Unit Test (Assume 256 bytes make a block)
// //
// // ****************************************************************************
// class WinbondBlockEraseTest : public WinbondWriteTest {
// 	virtual void SetUp() {
// 		std::fill(mem, mem + 1024, 0x00); // fill with all 0x00
// 		std::fill(ref_mem, ref_mem + 1024, 0x00); // fill with all 0x00
// 	}

// 	virtual void TearDown() {}
// };

// TEST_P(WinbondBlockEraseTest, AddressOnPageBound) {
// 	// block_erase_64KB(0);
// 	my_spi_erase(0, 256);

// 	// Construct reference mem to compare to
// 	// uint8_t src[256];
// 	std::fill(ref_mem, ref_mem + 256, 0xFF);
// 	// construct_ref_mem(0, 256, src, ref_mem);

// 	// print_mem(mem);
// 	// print_mem(read_buf);

// 	EXPECT_EQ(mem[GetParam()], ref_mem[GetParam()]);
// }

// TEST_P(WinbondBlockEraseTest, AddressNotOnPageBound) {
// 	// block_erase_64KB(128);
// 	my_spi_erase(128, 512);

// 	// Construct reference mem to compare to
// 	// uint8_t src[512];
// 	std::fill(ref_mem, ref_mem + 512, 0xFF);
// 	// construct_ref_mem(0, 512, src, ref_mem);

// 	// print_mem(mem);
// 	// print_mem(read_buf);

// 	EXPECT_EQ(mem[GetParam()], ref_mem[GetParam()]);
// }

// INSTANTIATE_TEST_CASE_P(SetupFlashAddressArray,
//                         WinbondBlockEraseTest,
//                         Range(0, 1024));

// ****************************************************************************
//
// Winbond Chip Select Unit Test
//
// ****************************************************************************
uint32_t gpio_porta_base[1024];

// Point the replacement GPIO_PORTA_BASE to the base address 
// of the virtual register space in RAM (gpio_porta_base)
volatile uint32_t GPIO_PORTA_BASE = (uint32_t) gpio_porta_base;

class WinbondChipSelectTest : public ::testing::Test {
	virtual void SetUp() {
		gpio_porta_base[4096] = {0x00};
		// printf("GPIO_PORTA_BASE is at %#02x\n", &gpio_porta_base[0]);
		// printf("gpio_porta_base[8] is at %#02x\n", &gpio_porta_base[8]);
	}

	virtual void TearDown() {}

	// // Visualize virtual register space in RAM
	// void print_reg_space(uint8_t *reg_space_buf) {

	// }
};

TEST_F(WinbondChipSelectTest, CheckRegisterValue) {
	chip_select(1);

	EXPECT_EQ(gpio_porta_base[8], 1);
}