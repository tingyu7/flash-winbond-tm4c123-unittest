#include "spi_driver.h"

// uint32_t g_rx_buf = 0;
// volatile uint32_t g_wait_for_int = 0;

//*****************************************************************************
//
// Initialize SPI
//
//*****************************************************************************
// void init_spi(void) {
//     SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);

//     // Wait for the Peripheral to be ready for programming
//     while (!SysCtlPeripheralReady(SYSCTL_PERIPH_SSI0));

//     SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

//     // Wait for the Peripheral to be ready for programming
//     while (!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOA));

//     // SSI pin configuration
//     GPIOPinConfigure(GPIO_PA2_SSI0CLK);
//     GPIOPinConfigure(GPIO_PA4_SSI0RX);
//     GPIOPinConfigure(GPIO_PA5_SSI0TX);
//     GPIOPinTypeSSI(GPIO_PORTA_BASE,
//                    GPIO_PIN_5 | GPIO_PIN_4 | GPIO_PIN_2);

//     // Configure SSI operation mode
//     SSIConfigSetExpClk(SSI0_BASE,
//                        SysCtlClockGet(),
//                        SSI_FRF_MOTO_MODE_0,
//                        SSI_MODE_MASTER,
//                        1000000,
//                        8);

//     // SSI CS pin configuration
//     GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_3);
//     GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3, GPIO_PIN_3);

//     // Enable SSI interface (post configuration)
//     SSIEnable(SSI0_BASE);

//     // Read any residual data from the SSI port.
//     while (SSIDataGetNonBlocking(SSI0_BASE, &g_rx_buf));

//     // Disable and clearing interrupt flagss
//     SSIIntDisable(SSI0_BASE, SSI_TXFF | SSI_RXFF | SSI_RXTO | SSI_RXOR);
//     SSIIntClear(SSI0_BASE, SSI_TXFF | SSI_RXFF | SSI_RXTO | SSI_RXOR);

//     // Enable End of Transmit interrupt mode for the TXRIS interrupt.
//     HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_EOT;

//     // Enable the SSI interrupt.
//     IntEnable(INT_SSI0);

//     // Enable processor interrupts.
//     IntMasterEnable();
// }

// //*****************************************************************************
// //
// // Transfer data bytes between MCU and slave device
// //
// //*****************************************************************************
// uint8_t byte_transfer(uint8_t data) {
//     g_wait_for_int = 1;

//     // Enable the EOT interrupts. This will start the actual
//     // transfer.
//     SSIIntEnable(SSI0_BASE, SSI_TXFF);
//     SSIDataPutNonBlocking(SSI0_BASE, (uint32_t)data);

//     // Interrupt will trigger here

//     // Wait until end of a data byte exchange (TX & RX)
//     while (g_wait_for_int);

//     return (uint8_t)g_rx_buf;
// }

// //*****************************************************************************
// //
// // SSI0 interrupt handler
// //
// //*****************************************************************************
// void SSI0IntHandler(void) {
//     // Entering here means a data byte has been transmitted from the TX FIFO
//     // to the slave and there is data available on the RX FIFO to be read
//     g_wait_for_int = 0;
//     SSIIntClear(SSI0_BASE, SSI_TXFF);
//     SSIDataGetNonBlocking(SSI0_BASE, &g_rx_buf);
//     SSIIntDisable(SSI0_BASE, SSI_TXFF);
// }
